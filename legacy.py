#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import requests
import I2C_LCD_driver
import datetime
import threading
from time import gmtime, strftime
from datetime import datetime,tzinfo,timedelta
import datetime

class Zone(tzinfo):
    def __init__(self,offset,isdst,name):
        self.offset = offset
        self.isdst = isdst
        self.name = name
    def utcoffset(self, dt):
        return timedelta(hours=self.offset) + self.dst(dt)
    def dst(self, dt):
            return timedelta(hours=1) if self.isdst else timedelta(0)
    def tzname(self,dt):
         return self.name

GMT = Zone(6,False,'GMT')
mylcd = I2C_LCD_driver.lcd()
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Triger detecter PIN
GPIO.setup(27, GPIO.OUT, initial=GPIO.HIGH) # LED pin RED
GPIO.setup(22, GPIO.OUT, initial=GPIO.HIGH) # LED pin GREEN
GPIO.setup(17, GPIO.OUT, initial=GPIO.HIGH) # Door pin


fontdata1 = [
        # char(0) - P
        [ 0b11111,
          0b10001,
          0b10001,
          0b10001,
          0b10001,
          0b10001,
          0b10001,
          0b00000 ],
        # char(1) - D

        [ 0b00110,
          0b01010,
          0b01010,
          0b01010,
          0b11111,
          0b10001,
          0b10001,
          0b00000],
         # char(2) - SH

        [ 0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b11111,
          0b00000],
        # char(3) - SH'
        [ 0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b10101,
          0b11111,
          0b00001],

        # char(4) - L
        [ 0b00100,
	  0b01010,
	  0b10001,
	  0b10001,
	  0b10001,
	  0b10001,
	  0b10001,
	  0b00000],

	# char(5) - '
	[ 0b10000,
	  0b10000,
	  0b10000,
	  0b11110,
	  0b10001,
	  0b10001,
	  0b11110,
	  0b00000],
        # char(6) - ZH
       [ 0b10101,
         0b10101,
         0b10101,
         0b11111,
         0b10101,
         0b10101,
         0b10101,
         0b00000],
       # char(7) - I
      [ 0b10001,
        0b10011,
        0b10101,
        0b11001,
        0b10001,
        0b10001,
        0b10001,
        0b10001]
]

mylcd.lcd_load_custom_chars(fontdata1)

def open_door():
    # GPIO.setup(17, GPIO.OUT) # Door pin
    GPIO.output(17, False)   # Opens Door
    time.sleep(0.7)          # Wait for 1.5 second
    GPIO.output(17, True)    # Close the door

def led_strip_RED_ON():
    # GPIO.setup(27, GPIO.OUT) # LED pin RED
    GPIO.output(27, False)   # Turn On RED LED


def led_strip_GREEN_ON():
    # GPIO.setup(22, GPIO.OUT) # LED pin GREEN
    GPIO.output(22, False)   # Turn On GREEN LED

def led_strip_RED_OFF():
    GPIO.output(27, True)   # Turn OFF RED LED

def led_strip_GREEN_OFF():
    GPIO.output(22, True)   # Turn OFF GREEN LED


def print_lcd_GO(N):
    mylcd.lcd_write(0x80)
    mylcd.lcd_write_char(0) # p
    mylcd.lcd_display_string("POXO",1,1)
    mylcd.lcd_write_char(1) # d
    mylcd.lcd_display_string(" PA3PE", 1,6)
    mylcd.lcd_write_char(2) # sh
    mylcd.lcd_display_string("EH", 1, 13 )

    mylcd.lcd_display_string("OCTA", 2, 0 )
    mylcd.lcd_write_char(4) # l
    mylcd.lcd_display_string("OC",2 ,5)
    mylcd.lcd_write_char(5) # '
    mylcd.lcd_display_string(": ", 2, 8 )
    mylcd.lcd_display_string(N, 2, 10)




def print_lcd_NOGO():
    mylcd.lcd_write(0x80)
    mylcd.lcd_write_char(0)
    mylcd.lcd_display_string("POXO",1,1)
    mylcd.lcd_write_char(1)
    mylcd.lcd_display_string(" 3A",1,6)
    mylcd.lcd_write_char(0)
    mylcd.lcd_display_string("PE",1 , 10)
    mylcd.lcd_write_char(3)
    mylcd.lcd_display_string("EH", 1, 13 )

def print_lcd_WAIT():
    mylcd.lcd_write_char(0)
    mylcd.lcd_display_string("O",1,1)
    mylcd.lcd_write_char(1)
    mylcd.lcd_display_string("O",1,3)
    mylcd.lcd_write_char(6)
    mylcd.lcd_write_char(1)
    mylcd.lcd_write_char(7)
    mylcd.lcd_display_string("TE...",1 , 7)

def print_lcd_WEL():
    mylcd.lcd_display_string("QOS' KELDIN'IZ")

def print_lcd_NOCON():
    mylcd.lcd_display_string("BAI'LANYS ZHOQ")

def clear_lcd():
    mylcd.lcd_clear()

def write_id(id):
    log.write(id)


def mmain():
    wait_time = 9000

    while(1):
        print_lcd_WEL()
        id_number = input("Put card or barcode:  ")
        clear_lcd()
        print_lcd_WAIT()
        if(id_number.find("[") != -1):
            id_number = id_number[3:len(id_number)]
        if any(c.isalpha() for c in id_number):
            request_txt = 'http://192.168.1.200/api/values/c%s' % id_number
            type='Карта'
            for_post = 'c%s' % id_number
        else:
            request_txt = 'http://192.168.1.200/api/values/t%s' % id_number
            type='билет'
            for_post = 't%s' % id_number
        connection = 0
        try:
            r = requests.get(request_txt, timeout = 5)
            print('%s тип носителя: %s оставшихся проходов:%s ; %s' % (id_number, type, r.text, datetime.datetime.now(GMT).strftime('%m/%d/%Y %H:%M:%S %Z')))

            connection = 1
        except:
            connection = 0
        clear_lcd()
        if(connection == 1):
            if r.status_code == 200:
                clear_lcd()
                if int(r.text) > 0:
                    N = str( int(r.text) - 1 )
                    print_lcd_GO(N)
                    open_door()
                    c = 0
                    detected = 0
                    while(c<wait_time):
                        if c % 500 == 0:
                            led_strip_GREEN_ON()
                        c=c+1
                        if  c % 1000 == 0:
                            led_strip_GREEN_OFF()
                            c=c+1
                        if GPIO.input(26) == GPIO.LOW:
                            detected = 1
                            print("Button was pushed!")
                            break
                        time.sleep(0.001)
                    led_strip_GREEN_OFF()
                    clear_lcd()
                    if detected == 1:
                        try:

                            r = requests.post("http://192.168.1.200/api/values/", data={'tickorderid': str(for_post)}, timeout = 5)
                        except:
                            print("tttt")

                else:
                    print("NO CREDITS")
                    led_strip_RED_ON()
                    print_lcd_NOGO()

                    time.sleep(2)

                    led_strip_RED_OFF()
                    clear_lcd()
            else:
                clear_lcd()
                print("Invalid card")
                print_lcd_NOGO()
                led_strip_RED_ON()
                time.sleep(2)
                led_strip_RED_OFF()
                clear_lcd()
        else:
            print("NOOOOO CONNECTIOOOOOON")
            print_lcd_NOCON()
            open_door()
            c = 0
            detected = 0
            while(c < wait_time):
                if c % 500 == 0:
                    led_strip_GREEN_ON()
                    led_strip_RED_ON()
                c=c+1
                if  c % 1000 == 0:
                    led_strip_GREEN_OFF()
                    led_strip_RED_OFF()
                    c=c+1


                if GPIO.input(26) == GPIO.LOW:
                    detected = 1
                    print("Button was pushed!")
                    break
                time.sleep(0.001)

            clear_lcd()
            led_strip_RED_OFF()
            led_strip_GREEN_OFF()
            if detected == 1:
                try:
                    file = open("log.txt","a")
                    file.write(id_number+"\n")
                    file.close()
                except:
                    print("NOT ABLE TO OPEN THE FILE")
                    led_strip_GREEN_ON()
                    time.sleep(1)
                    led_strip_GREEN_OFF()


def upload_id():
    while(1):
        dd = datetime.datetime.now()
        if dd.hour >= 16 and dd.hour < 18:
        #if dd.hour >0:
            testflag = 0
            try:
                file = open("log.txt", "r")
                testflag = 1
            except:
                print("CANNOT OPEN FILE")
                testflag= 0
            if testflag == 1:
                for line in file:
                    line = line[:len(line)-1]
                    print("send:"+line+" END")
                    check = 0
                    while(check == 0):
                        try:
                            rr = requests.post("http://192.168.1.200/api/values/", data={'tickorderid': str(line)}, timeout = 10)
                            time.sleep(1)
                            check =1
                        except:
                            print("cannot sendddddddd")
                            check = 0
                    print("1112222222222222222222222222222")
                file.close()
                time.sleep(10)
                try:
                    file = open("log.txt","w").close()
                except:
                    print("CAN NOT WRITE")
        time.sleep(10)

threading.Thread(target = mmain).start()
threading.Thread(target = upload_id).start()
